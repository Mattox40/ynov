# TP4 : Vers un réseau d'entreprise

# I. Dumb switch

## 3. Setup topologie 1

#### 🌞 Commençons simple

```bash
# On définit les deux adresses statiques de nos VPCS
# Pour notre PC 1
VPCS> ip 10.1.1.1/24
Checking for duplicate address...
VPCS : 10.1.1.1 255.255.255.0

# Pour notre PC 2
PC2> ip 10.1.1.2/24
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0

# On ping PC 1 depuis PC 2
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=13.189 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=9.290 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=12.859 ms
```

# II. VLAN

## 3. Setup topologie 2

#### 🌞 Adressage

```bash
# On a donc PC1 et PC2 qui ont leurs IP
# PC 1
VPCS> show ip

NAME        : VPCS[1]
IP/MASK     : 10.1.1.1/24
(...)

# PC 2
PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.1.1.2/24
(...)

# Et on donne une IP à PC3
PC3> ip 10.1.1.3/24
Checking for duplicate address...
PC3 : 10.1.1.3 255.255.255.0

# On regarde si tous le monde pings
# PC1 vers PC2
VPCS> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=101.437 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=25.225 ms
84 bytes from 10.1.1.2 icmp_seq=3 ttl=64 time=23.422 ms

# PC2 vers PC3
PC2> ping 10.1.1.3

84 bytes from 10.1.1.3 icmp_seq=1 ttl=64 time=38.804 ms
84 bytes from 10.1.1.3 icmp_seq=2 ttl=64 time=55.912 ms
84 bytes from 10.1.1.3 icmp_seq=3 ttl=64 time=50.159 ms

# PC3 vers PC1
PC3> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=58.558 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=68.515 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=47.774 ms
```

#### 🌞 Configuration des VLANs

```
# On commence par enable le switch
Switch>enable

# On déclare les VLAN
# VLAN 10
Switch#conf t
Switch(config)#vlan 10
Switch(config-vlan)#name admins
Switch(config-vlan)#exit

# VLAN 20
Switch(config)#vlan 20
Switch(config-vlan)#name guests
Switch(config-vlan)#exit

# On affiche nos VLAN
Switch#show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/0, Gi0/1, Gi0/2, Gi0/3
                                                Gi1/0, Gi1/1, Gi1/2, Gi1/3
                                                Gi2/0, Gi2/1, Gi2/2, Gi2/3
                                                Gi3/0, Gi3/1, Gi3/2, Gi3/3
10   admins                           active
20   guests                           active
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup

# On configure l'interface Gi0/0 en VLAN 10
Switch(config)#interface Gi0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10


# On configure l'interface Gi0/1 en VLAN 10
Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10

# On configure l'interface Gi0/2 en VLAN 20
Switch(config)#interface Gi0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 20

# On observe donc les changements
Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
(...)
10   admins                           active    Gi0/0, Gi0/1
20   guests                           active    Gi0/2
(...)

# On essaye de ping par les différents clients
# PC2 vers PC1
PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=8.091 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=10.922 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=8.670 ms

# PC3 vers PC1 et PC2
PC3> ping 10.1.1.2

host (10.1.1.2) not reachable

PC3> ping 10.1.1.1

host (10.1.1.1) not reachable
# Les VLAN sont bien configurés
```

# III. Routing

## 3. Setup topologie 3

#### 🌞 Adressage

```
# PC1 garde son IP
VPCS> show ip

NAME        : VPCS[1]
IP/MASK     : 10.1.1.1/24
(...)

# PC2 aussi
PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.1.1.2/24

# On definit l'adresse IP de adm1
adm1> ip 10.2.2.1/24
Checking for duplicate address...
adm1 : 10.2.2.1 255.255.255.0

adm1> show ip

NAME        : adm1[1]
IP/MASK     : 10.2.2.1/24

# On a l'adresse IP de web1
[mattox@web1 ~]$ ip a
(...)
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:5a:e0:e7 brd ff:ff:ff:ff:ff:ff
    inet 10.3.3.1/24 brd 10.3.3.255 scope global noprefixroute enp0s3
       valid_lft forever preferred_lft forever
```

#### 🌞 Configuration des VLANs

```
# On definit les VLANs
Switch(config)#vlan 11
Switch(config-vlan)#name clients
Switch(config-vlan)#exit

Switch(config)#vlan 12
Switch(config-vlan)#name admins
Switch(config-vlan)#exit

Switch(config)#vlan 13
Switch(config-vlan)#name servers
Switch(config-vlan)#exit

# On a donc nos 3 vlan
Switch#show vlan

11   clients                          active
12   admins                           active
13   servers                          active

# On donne le VLAN 11 aux deux interfaces de clients
Switch(config)#interface Gi0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit

Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit

# On donne le VLAN 12 a l'interface de admins
Switch(config)#interface Gi0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 12
Switch(config-if)#exit

# On donne le VLAN 13 à l'interface de servers
Switch(config)#interface Gi0/3
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 13
Switch(config-if)#exit

# On a donc 3 VLAN
Switch#show vlan
(...)
11   clients                          active    Gi0/0, Gi0/1
12   admins                           active    Gi0/2
13   servers                          active    Gi0/3

# On configure le port Gi1/0 du switch en mode trunk
Switch(config)#interface Gi1/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11,12,13

Switch#show interface trunk
Port        Mode             Encapsulation  Status        Native vlan
Gi1/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi1/0       1-4094

Port        Vlans allowed and active in management domain
Gi1/0       1,11-13

Port        Vlans in spanning tree forwarding state and not pruned
Gi1/0       none
```

#### 🌞 Config du routeur

```
# On configure notre Routeur
# Pour le VLAN 11
R1(config)#interface fastEthernet 0/0.11
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0

# Pour le VLAN 12 
R1(config)#interface fastEthernet 0/0.12
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0

# Pour le VLAN 13
R1(config)#interface fastEthernet 0/0.13
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0

R1#show vlans
Virtual LAN ID:  11 (IEEE 802.1Q Encapsulation)

   vLAN Trunk Interface:   FastEthernet0/0.11

   Protocols Configured:   Address:              Received:        Transmitted:
           IP              10.1.1.254                   0                   0

Virtual LAN ID:  12 (IEEE 802.1Q Encapsulation)

   vLAN Trunk Interface:   FastEthernet0/0.12

   Protocols Configured:   Address:              Received:        Transmitted:
           IP              10.2.2.254                   0                   0

Virtual LAN ID:  13 (IEEE 802.1Q Encapsulation)

   vLAN Trunk Interface:   FastEthernet0/0.13

   Protocols Configured:   Address:              Received:        Transmitted:
           IP              10.3.3.254                   0                   0

# On n'oublie pas d'activer l'interface du Routeur
R1(config)#interface fastEthernet 0/0
R1(config-if)#no shut
```

#### 🌞 Vérif 

```
## On tente de ping le router
# Avec PC1
VPCS> ping 10.1.1.254

10.1.1.254 icmp_seq=1 timeout
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=13.422 ms
84 bytes from 10.1.1.254 icmp_seq=3 ttl=255 time=15.031 ms
84 bytes from 10.1.1.254 icmp_seq=4 ttl=255 time=16.449 ms

# Avec adm1
adm1> ping 10.2.2.254

84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=20.080 ms
84 bytes from 10.2.2.254 icmp_seq=2 ttl=255 time=24.027 ms
84 bytes from 10.2.2.254 icmp_seq=3 ttl=255 time=17.075 ms

# Avec web1
[mattox@web1 ~]$ ping 10.3.3.254
PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=18.4 ms
64 bytes from 10.3.3.254: icmp_seq=2 ttl=255 time=11.9 ms
64 bytes from 10.3.3.254: icmp_seq=3 ttl=255 time=16.4 ms

## On definit les routes par défaut en leur ajouter une gateway
# Pour PC1
VPCS> ip 10.1.1.1/24 10.1.1.254
Checking for duplicate address...
VPCS : 10.1.1.1 255.255.255.0 gateway 10.1.1.254

# Pour PC2
PC2> ip 10.1.1.2/24 10.1.1.254
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0 gateway 10.1.1.254

# Pour adm1
adm1> ip 10.2.2.1/24 10.2.2.254
Checking for duplicate address...
adm1 : 10.2.2.1 255.255.255.0 gateway 10.2.2.254

# Pour web1
[mattox@web1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3 | grep GATEWAY
GATEWAY=10.3.3.254

## On ping pour voir si nos routes fonctionnent
# De PC1 vers adm1
VPCS> ping 10.2.2.1

84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=47.013 ms
84 bytes from 10.2.2.1 icmp_seq=2 ttl=63 time=47.818 ms
84 bytes from 10.2.2.1 icmp_seq=3 ttl=63 time=40.803 ms

# De adm1 vers web1
adm1> ping 10.3.3.1

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=19.451 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=16.341 ms
```

# IV. NAT

## 3. Setup topologie 4

#### 🌞 Ajoutez le noeud Cloud à la topo

> On branche le cloud à eth1

```
# On configure l'interface du routeur en DHCP
R1(config)#interface fastEthernet 1/0
R1(config-if)#ip adress dhcp
R1(config-if)#no shut

# Notre interface est bien en DHCP
R1#show ip int br
(...)
FastEthernet1/0            10.0.3.16       YES DHCP   up                    up

# On essaye de ping 1.1.1.1 depuis le PC1
VPCS> ping 1.1.1.1

84 bytes from 1.1.1.1 icmp_seq=1 ttl=55 time=45.410 ms
84 bytes from 1.1.1.1 icmp_seq=2 ttl=55 time=40.114 ms
84 bytes from 1.1.1.1 icmp_seq=3 ttl=55 time=47.891 ms
# Cela marche parfaitement
```

#### 🌞 Configurez le NAT

```
# On configure les interfaces en interne ou externe
R1(config)#interface fastEthernet 0/0
R1(config-if)#ip nat inside

R1(config)#interface fastEthernet 1/0
R1(config-if)#ip nat outside

# On definit une liste où tout le trafic est autorisé
R1(config)#access-list 1 permit any

# On configure le NAT
R1(config)#ip nat inside source list 1 interface fastEthernet 1/0 overload
```

#### 🌞 Test

```
# Les routes par défaut sont déjà établies

# On définit un DNS sur les VPCS et web1
# Pour PC1
VPCS> ip dns 1.1.1.1

# Pour PC2
PC2> ip dns 1.1.1.1

# Pour adm1
adm1> ip dns 1.1.1.1

# Pour web1
[mattox@web1 ~]$ sudo vim /etc/sysconfig/network-scripts/ifcfg-enp0s3
[mattox@web1 ~]$ sudo cat /etc/sysconfig/network-scripts/ifcfg-enp0s3 | grep DNS1
DNS1=1.1.1.1

## On essaye de ping google.com
# Depuis PC1
VPCS> ping google.com
google.com resolved to 216.58.204.110

84 bytes from 216.58.204.110 icmp_seq=1 ttl=113 time=57.573 ms
84 bytes from 216.58.204.110 icmp_seq=2 ttl=113 time=57.197 ms
84 bytes from 216.58.204.110 icmp_seq=3 ttl=113 time=52.412 ms

# ou depuis adm1
adm1> ping google.com
google.Com resolved to 216.58.209.238

84 bytes from 216.58.209.238 icmp_seq=1 ttl=113 time=60.180 ms
84 bytes from 216.58.209.238 icmp_seq=2 ttl=113 time=58.890 ms
84 bytes from 216.58.209.238 icmp_seq=3 ttl=113 time=49.469 ms
```

# V. Add a building

> On garde notre config précédente, et on ajoute le switch3 et les clients en dhcp.client1.tp3

#### 🌞  Vous devez me rendre le show running-config de tous les équipements

=> [R1.conf]('./../ressources/r1.conf')

=> [sw1.conf]('./../ressources/sw1.conf')

=> [sw2.conf]('./../ressources/sw2.conf')

=> [sw3.conf]('./../ressources/sw3.conf')

#### 🌞  Mettre en place un serveur DHCP dans le nouveau bâtiment
```
## On configure le DHCP, donc le fichier /etc/dhcp/dhcpd.conf
[mattox@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
subnet 10.1.1.0 netmask 255.255.255.0 {
   range 10.1.1.3 10.1.1.252; 
   option domain-name-servers 1.1.1.1;
   option subnet-mask 255.255.255.0;
   option routers 10.1.1.254;
}

# Et on démarre le dhcpd
[mattox@dhcp ~]$ sudo systemctl start dhcpd.service
```

#### 🌞  Vérification

```
# PC3 recupere son ip en DHCP
PC3> ip dhcp
DDORA IP 10.1.1.3/24 GW 10.1.1.254

# PC4 de même
PC4> ip dhcp
DDORA IP 10.1.1.4/24 GW 10.1.1.254

# PC3 peut ping 8.8.8.8
PC3> ping 8.8.8.8

84 bytes from 8.8.8.8 icmp_seq=1 ttl=115 time=76.713 ms
84 bytes from 8.8.8.8 icmp_seq=2 ttl=115 time=73.352 ms
84 bytes from 8.8.8.8 icmp_seq=3 ttl=115 time=61.867 ms

# PC4 peut ping google.com
PC4> ping google.com
google.com resolved to 216.58.206.238

84 bytes from 216.58.206.238 icmp_seq=1 ttl=113 time=72.366 ms
84 bytes from 216.58.206.238 icmp_seq=2 ttl=113 time=75.920 ms

# PC3 peut ping web1
PC3> ping 10.3.3.1

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=59.115 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=62.736 ms
```