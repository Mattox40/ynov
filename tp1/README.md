#   I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

* Pour afficher le nom, l'adresse MAC et l'adresse IP de l'interface Wifi, il faut taper la commande `ipconfig /all`

Comme ceci : 
```
C:\Users\matte>ipconfig /all
(...)
Carte réseau sans fil Wi-Fi :

   Adresse physique . . . . . . . . . . . : 50-E0-85-DB-61-8B
(...)
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.1.172(préféré)
```
On peut donc voir que le nom est `sans fil Wi-Fi`, l'adresse MAC `50-E0-85-DB-61-8B` et l'adresse IP `10.33.1.172`

* Les informations de la carte Ethernet se trouvent aussi avec la commande `ipconfig /all`

```
Carte Ethernet Ethernet :

   Statut du média. . . . . . . . . . . . : Média déconnecté
(...)
   Adresse physique . . . . . . . . . . . : 04-D4-C4-E6-15-34
```
On peut donc voir que le nom est `Ethernet Ethernet`, l'adresse MAC `04-D4-C4-E6-15-34` et l'adresse IP n'existe pas car le port Ethernet n'est pas branché

* l'adresse IP de la passerelle de notre carte WiFi est visible grâce à la commande `ipconfig`

```
Carte réseau sans fil Wi-Fi :
(...)
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```
L'adresse IP est donc `10.33.3.253`

* Pour trouver l'IP, la MAC et la gateway pour l'interface WiFi de mon PC (Windows) : 

il suffit d'aller dans les **Paramètres de l'ordinateur** -> **Réseau et Internet** -> **Etat** -> **Afficher les propriétés du matériel et de la connexion**

![](./img/adresse_IP_passerelle.png)

###    Questions

* La gateaways d'Ynov sert à se connecter à des réseaux extérieures à YNOV, à se connecter au reste du monde en autres 


## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)

#### Changement de l'adresse IP

* Mon IP a changé car j'ai fait cette partie chez moi, elle sera donc `192.168.1.100`

Pour la changer, il faut aller dans : **Panneau de configuration** / **Afficher l'état et la gestion du réseau** /  **Modifier les parametres de la carte** / **Wifi** / **Propriétés** / **Selectionner le `Protocole Internet version 4 (TCP/IPv4)`** / **Propriétés**

![](./img/adresse_IP_Manuel.png)

On peut ainsi changer l'adresse IP sur cette interface, nous remettons le même masque et la même passerelle, mais en changeant l'adresse IP de `192.168.1.100` en `192.168.1.90`

On peut voir en faisant un `ipconfig` dans le terminal que le changement est bien effectué : 

```
Carte réseau sans fil Wi-Fi :
(...)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.90
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . : 192.168.1.254
```

* La connexion s'est effectivement coupée, en effet, l'adresse IP que j'ai choisi est déjà prise.

### B. Table ARP

* Pour afficher la table ARP, on peut utiliser la commande `arp -a`

Mon IP est `10.33.1.172`

```
C:\Users\matte>arp -a

Interface : 10.33.1.172 --- 0x3
  Adresse Internet      Adresse physique      Type
  10.33.1.39            70-66-55-dd-96-4f     dynamique
  10.33.1.89            ac-67-5d-00-06-45     dynamique
  10.33.1.93            b8-9a-2a-3d-c1-1a     dynamique
  10.33.1.96            78-2b-46-e5-17-62     dynamique
  10.33.2.193           e0-2b-e9-7d-a6-c2     dynamique
  10.33.3.179           94-e7-0b-0a-46-aa     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  10.33.19.254          00-12-00-40-4c-bf     dynamique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
(...)
Interface : 192.168.231.1 --- 0x14
  Adresse Internet      Adresse physique      Type
  192.168.231.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  232.236.18.4          01-00-5e-6c-12-04     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

* On peut voir l'adresse MAC de la passerelle `00-12-00-40-4c-bf` à l'aide de son Adresse IP `10.33.3.253`.

* On envoie des pings sur certaines adresses IP : 

```
C:\Users\matte>ping 10.33.2.222

Envoi d’une requête 'Ping'  10.33.2.222 avec 32 octets de données :
Réponse de 10.33.2.222 : octets=32 temps=368 ms TTL=64
Réponse de 10.33.2.222 : octets=32 temps=32 ms TTL=64
Réponse de 10.33.2.222 : octets=32 temps=526 ms TTL=64

Statistiques Ping pour 10.33.2.222:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 32ms, Maximum = 526ms, Moyenne = 308ms
Ctrl+C
^C
C:\Users\matte>ping 10.33.3.219

Envoi d’une requête 'Ping'  10.33.3.219 avec 32 octets de données :
Réponse de 10.33.3.219 : octets=32 temps=16 ms TTL=64
Réponse de 10.33.3.219 : octets=32 temps=29 ms TTL=64
Réponse de 10.33.3.219 : octets=32 temps=48 ms TTL=64

Statistiques Ping pour 10.33.3.219:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 16ms, Maximum = 48ms, Moyenne = 31ms
Ctrl+C
^C
C:\Users\matte>ping 10.33.3.232

Envoi d’une requête 'Ping'  10.33.3.232 avec 32 octets de données :
Réponse de 10.33.3.232 : octets=32 temps=100 ms TTL=64
Réponse de 10.33.3.232 : octets=32 temps=10 ms TTL=64
Réponse de 10.33.3.232 : octets=32 temps=93 ms TTL=64

Statistiques Ping pour 10.33.3.232:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 10ms, Maximum = 100ms, Moyenne = 67ms
Ctrl+C
^C
```

Nous avons envoyé des pings sur les adresses `10.33.2.222`, `10.33.3.219`,`10.33.3.232` 

Si on affiche la table ARP : 

```
C:\Users\matte>arp -a

Interface : 10.33.1.172 --- 0x3
  Adresse Internet      Adresse physique      Type
  10.33.0.132           d8-f3-bc-c0-c4-ef     dynamique
  10.33.0.167           2c-8d-b1-d9-6c-55     dynamique
  10.33.0.175           74-d8-3e-f2-cc-95     dynamique
  10.33.0.232           b4-6d-83-d2-d4-80     dynamique
  10.33.1.39            70-66-55-dd-96-4f     dynamique
  10.33.1.62            dc-fb-48-c8-b1-4e     dynamique
  10.33.1.93            b8-9a-2a-3d-c1-1a     dynamique
  10.33.1.96            78-2b-46-e5-17-62     dynamique
  10.33.1.162           d0-c5-d3-ce-b1-79     dynamique
  10.33.2.41            48-a4-72-41-c6-d4     dynamique
  10.33.2.94            70-9c-d1-03-ec-4c     dynamique
  10.33.2.193           e0-2b-e9-7d-a6-c2     dynamique
  10.33.2.222           88-bf-e4-cb-d9-46     dynamique
  10.33.3.2             14-4f-8a-65-8c-c7     dynamique
  10.33.3.4             74-d8-3e-3e-99-41     dynamique
  10.33.3.17            80-32-53-e2-78-04     dynamique
  10.33.3.24            ac-12-03-2e-e4-92     dynamique
  10.33.3.179           94-e7-0b-0a-46-aa     dynamique
  10.33.3.219           a0-78-17-b5-63-bb     dynamique
  10.33.3.226           f0-77-c3-06-8f-42     dynamique
  10.33.3.232           88-66-5a-4d-a7-f5     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  10.33.19.254          00-12-00-40-4c-bf     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  232.236.18.4          01-00-5e-6c-12-04     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
(...)
Interface : 192.168.231.1 --- 0x14
  Adresse Internet      Adresse physique      Type
  192.168.231.255       ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  232.236.18.4          01-00-5e-6c-12-04     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```

Je peux reperer les 3 adresses macs que l'on a ping : 

```
10.33.2.222           88-bf-e4-cb-d9-46     dynamique
(...)
10.33.3.219           a0-78-17-b5-63-bb     dynamique
(...)
10.33.3.232           88-66-5a-4d-a7-f5     dynamique
```

### C. `nmap`

* Nous lancons un scan de ping sur le réseau d'YNOV avec la commande `nmap -sP 10.33.0.0/22`

```
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 14:54 Paris, Madrid (heure d’été)
Nmap scan report for 10.33.0.10
Host is up (2.1s latency).
MAC Address: B0:6F:E0:4C:CF:EA (Samsung Electronics)
Nmap scan report for 10.33.0.13
Host is up (0.052s latency).
MAC Address: D2:50:0E:26:94:8B (Unknown)
Nmap scan report for 10.33.0.21
Host is up (0.013s latency).
MAC Address: B0:FC:36:CE:9C:89 (CyberTAN Technology)
Nmap scan report for 10.33.0.27
Host is up (0.020s latency).
MAC Address: A8:64:F1:8B:1D:4D (Intel Corporate)
Nmap scan report for 10.33.0.31
Host is up (0.47s latency).
MAC Address: FA:C5:6D:60:4B:4C (Unknown)
Nmap scan report for 10.33.0.38
Host is up (0.0040s latency).
(...)
MAC Address: 00:1E:4F:F9:BE:14 (Dell)
Nmap scan report for 10.33.3.253
Host is up (0.013s latency).
MAC Address: 00:12:00:40:4C:BF (Cisco Systems)
Nmap scan report for 10.33.3.254
Host is up (0.011s latency).
MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.1.172
Host is up.
Nmap done: 1024 IP addresses (142 hosts up) scanned in 55.15 seconds
```

Le scan de ping a scanner 1024 adresses et a trouvé 142 IP occupés

On affiche alors la table ARP : 

```
C:\Users\matte>arp -a

Interface : 10.33.1.172 --- 0x3
  Adresse Internet      Adresse physique      Type
  10.33.0.67            dc-f5-05-db-cc-ab     dynamique
  10.33.0.71            f0-03-8c-35-fe-47     dynamique
  10.33.0.85            9e-08-36-e0-c5-22     dynamique
  10.33.0.132           d8-f3-bc-c0-c4-ef     dynamique
  10.33.0.142           5c-3a-45-5f-48-5d     dynamique
  10.33.0.143           f0-18-98-41-11-07     dynamique
  10.33.0.157           26-57-f1-a3-f2-89     dynamique
  10.33.0.166           7e-7a-66-16-64-ef     dynamique
  10.33.0.167           2c-8d-b1-d9-6c-55     dynamique
  10.33.0.175           74-d8-3e-f2-cc-95     dynamique
  10.33.0.232           b4-6d-83-d2-d4-80     dynamique
  10.33.1.39            70-66-55-dd-96-4f     dynamique
  10.33.1.62            dc-fb-48-c8-b1-4e     dynamique
  10.33.1.93            b8-9a-2a-3d-c1-1a     dynamique
  (...)
  10.33.3.219           a0-78-17-b5-63-bb     dynamique
  10.33.3.226           f0-77-c3-06-8f-42     dynamique
  10.33.3.232           88-66-5a-4d-a7-f5     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  10.33.19.254          00-12-00-40-4c-bf     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  232.236.18.4          01-00-5e-6c-12-04     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

### D. Modification d'adresse IP (part 2)

D'apres la question précédente, on prends l'adresse IP `10.33.1.75` qui est libre et pas repéré par le `nmap -sP 10.33.0.0/22`

* On fais la commande `nmap -sp 10.33.1.75` pour savoir si l'IP est libre 

```
C:\Users\matte>nmap -sP 10.33.1.75

Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-16 15:21 Paris, Madrid (heure d’été)
Note: Host seems down. If it is really up, but blocking our ping probes, try -Pn
Nmap done: 1 IP address (0 hosts up) scanned in 1.94 seconds
```

* On peux voir avec la commande `ipconfig` que mon adresse IP est `10.33.1.75` et que la passerelle est `10.33.3.253`

```
C:\Users\matte>ipconfig
(...)
Carte réseau sans fil Wi-Fi :

   Adresse IPv4. . . . . . . . . . . . . .: 10.33.1.75
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
```

* On peux aussi voir avec un ping sur l'adresse de Google `8.8.8.8`, qu'on as l'accès au DNS de Google

```
C:\Users\matte>ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=17 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=17 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=17 ms TTL=115

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 17ms, Maximum = 17ms, Moyenne = 17ms
```

## II. Exploration locale en duo

### 3. Modification d'adresse IP

* Nous avons modifier les parametres de cartes Ethernet comme ceci : 

```
C:\Users\matte>ipconfig

Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::e1db:4bc1:6c23:6d5f%11
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.0.1
```

Mon adresse IP est `192.168.0.2` et celle de mon camarade `192.168.0.1`

Nos masques `255.255.255.252` (\30)

Ma passerelle `192.168.0.1` et sa passerelle `192.168.0.2`

Lorsque qu'on tente de se ping, cela marche parfaitement.

```
C:\Users\matte>ping 192.168.0.1

Envoi d’une requête 'Ping'  192.168.0.1 avec 32 octets de données :
Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.0.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
```

On peut afficher la table ARP `arp -a`

```
C:\Users\matte>arp -a

Interface : 192.168.0.2 --- 0xb
  Adresse Internet      Adresse physique      Type
  192.168.0.1           a0-ce-c8-38-0c-17     dynamique
  192.168.0.3           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  232.239.18.4          01-00-5e-6f-12-04     statique
  232.243.34.4          01-00-5e-73-22-04     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```
On peut voir l'adresse `192.168.0.1` que l'on vient de ping

### 4. Utilisation d'un des deux comme gateway

* Une fois la démarche de partage de connexion par Ethernet effectué, on teste le ping des DNS: 

```
C:\Users\matte>ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=35 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=17 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=23 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=26 ms TTL=115

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 17ms, Maximum = 35ms, Moyenne = 25ms
C:\Users\matte>ping google.com

Envoi d’une requête 'ping' sur google.com [216.58.209.238] avec 32 octets de données :
Réponse de 216.58.209.238 : octets=32 temps=19 ms TTL=114
Réponse de 216.58.209.238 : octets=32 temps=19 ms TTL=114
Réponse de 216.58.209.238 : octets=32 temps=19 ms TTL=114
Réponse de 216.58.209.238 : octets=32 temps=20 ms TTL=114

Statistiques Ping pour 216.58.209.238:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 19ms, Maximum = 20ms, Moyenne = 19ms
```

Les deux ordinateurs ont bien la connexion

* Grâce à la commande `tracert`, nous pouvons voir le chemin de que parcours mon paquet au DNS `1.1.1.1`

```
C:\Users\maxmo>tracert 1.1.1.1
Détermination de l’itinéraire vers one.one.one.one [1.1.1.1]
avec un maximum de 30 sauts :
 1     2 ms     1 ms     1 ms  LAPTOP-UD8C09I0 [192.168.0.2]
 2     *        *        *     Délai d’attente de la demande dépassé.
 3     3 ms     2 ms     4 ms  10.33.3.253
 4   373 ms    45 ms    34 ms  10.33.10.254
 5     4 ms     2 ms     8 ms  reverse.completel.net [92.103.174.137]
 6    10 ms    10 ms    11 ms  92.103.120.182
 7    20 ms    19 ms    21 ms  172.19.130.117
 8    19 ms    19 ms    19 ms  46.218.128.74
 9    18 ms    18 ms    39 ms  equinix-paris.cloudflare.com [195.42.144.143]
10    18 ms    19 ms    18 ms  one.one.one.one [1.1.1.1]
Itinéraire déterminé.
```

### 5. Petit chat privé

* Sur le PC "Serveur" on tape la commande : `C:\Users\matte\netcat>nc.exe -l -p 8888` et sur le PC "Client" on tape la commande : `C:\Users\maxmo\netcat\netcat-1.11>nc.exe 192.168.0.2 8888`

On obtient alors un chat privé, où nous avons pu échanger quelque mots depuis nos deux machines 
Sur le pc de mon coequipier, il tape la commande suivante:
```
C:\Users\maxmo\netcat\netcat-1.11>nc.exe 192.168.0.2 8888
```
De mon coté (serveur), j'effectue ceci :
```
C:\Users\matte\netcat>nc.exe -l -p 8888
Test depuis 192.168.0.1
Test reçu depuis 192.168.0.2
```

* Pour aller plus loin, on peut communiquer uniquement sur la carte Ethernet,

Donc sur le pc de mon coequipier (serveur), il tape la commande : 
```
C:\Users\maxmo\netcat\netcat-1.11>nc.exe -l -p 9999 192.168.0.2
```

Et de mon coté (client), je tape la commande : 
```
C:\Users\matte\netcat>nc.exe 192.168.0.1 9999
Test depuis l'hôte
Test reçu depuis le guest
```

### 6. Firewall

#### Autorisez les pings

* On se rends dans **Les parametres avancées du pare-feu Windows** / 

* On crée une nouvelles règle de trafic entrant

* Type de règle -> Personalisée

* Programme -> Tous les programmes

* Protocole et ports -> ICMPv4 / Perso... : Certains types ICMP -> requete d'echo -> type 8

* Etendue -> Toutes adresses IP

* Action -> Autoriser la connexion 

* Profil -> Privé

Je la nomme `Autorisation ping aller`

Pour le retour, comme ce n'est pas le même type de  requête d'echo, je refais le même procédé sauf que pour la requête d'echo où je l'a met de **type 0** 

Lorsque j'essaye de ping mon coéquipier : 

```
C:\Users\matte\netcat>ping 192.168.0.1

Envoi d’une requête 'Ping'  192.168.0.1 avec 32 octets de données :
Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.0.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 192.168.0.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
```
Cela marche parfaitement

#### Autorisez le trafic sur le port qu'utilisee `nc`

Pareil que précédemment on va créer une nouvelle règle de traffic entrant : 

* Type de règles -> Ports
* Protocole et ports -> TCP / Ports locaux spécifique (8888)
* Action -> Autoriser la connexion
* Profil -> Privé

Nous allons à nouveau tenter de se parler avec le chat privé `nc` :

```
C:\Users\matte\netcat>nc.exe 192.168.0.1 8888
Test guest
Test bien reçu depuis l'hôte
```

Tout fonctionne parfaitement

## III. Manipulations d'autres outils/protocoles côté client

### 1. DHCP

On peut obtenir l'adresse IP du serveur DHCP à l'aide de la commande `ipconfig /all`

```
C:\Users\matte>ipconfig /all

(...)
Carte réseau sans fil Wi-Fi :
    (...)
   Bail obtenu. . . . . . . . . . . . . . : vendredi 17 septembre 2021 13:54:44
   Bail expirant. . . . . . . . . . . . . : vendredi 17 septembre 2021 20:24:48
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
```

L'adresse IP DHCP est `10.33.3.254`

La date d'expiration du bail est donc `vendredi 17 septembre 2021 20:24:48`

Le DHCP (Dynamic Host Configuration Protocol) est un protocole qui permet à un ordinateur qui se connecte sur un réseau local d’obtenir dynamiquement et automatiquement sa configuration IP 

#### 2. DNS

Etant chez moi, les adresses IP changeront.

* Pour trouver l'adresse IP du serveur DNS il suffit de faire la commande `ipconfig /all`

```
C:\Users\matte>ipconfig /all

Carte réseau sans fil Wi-Fi :
(...)
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.1.100(préféré)
    (...)
   Serveurs DNS. . .  . . . . . . . . . . : 192.168.1.254
```

* On cherche à faire un `nslookup` pour faire des requêtes DNS pour `google.com` et `ynov.com`

```
C:\Users\matte>nslookup google.com
Serveur :   bbox.lan
Address:  192.168.1.254

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4006:801::200e
          172.217.19.142


C:\Users\matte>nslookup ynov.com
Serveur :   bbox.lan
Address:  192.168.1.254

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```

On peut voir que l'adresse IP qui appartient au nom de domaine `google.com` est `172.217.19.142` et celle de `ynov.com` est `92.243.16.143` 

En effet, si on tape ces adresses IP sur un navigateur, on tombera sur les noms de domaines

* L'adresse IP du serveur à qui on vient d'envoyer des requetes

L'adresse IP a qui on vient d'envoyer les requetes est `192.168.1.254`, qui est mon serveur DNS

* On cherche à faire un `nslookup` pour les adresses `78.74.21.21` et `92.146.54.88` pour essayer de trouver un nom de domaine correspondant à ces adresses

```
C:\Users\matte>nslookup 78.74.21.21
Serveur :   bbox.lan
Address:  192.168.1.254

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21


C:\Users\matte>nslookup 92.146.54.88
Serveur :   bbox.lan
Address:  192.168.1.254

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```
D'après les résultats, ces adresses IP correspondent à des noms de domaines, `78.74.21.21` - `host-78-74-21-21.homerun.telia.com` et `92.146.54.88` - `apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr`

## IV. Wireshark

Un ping entre vous et la passerelle : 
(Mon adresse ip `192.168.1.100` et la passerelle `192.168.1.254`)

![](./img/Ping_Passerelle.png)

Connexion netcat : 
Etant chez moi pour cette partie, je n'ai pas pu le faire


Requête DNS à l'aide de la commande `nslookup google.com` : 

![](./img/DNS.png)

On peut voir l'adresse du DNS de Google `172.217.18.238`

