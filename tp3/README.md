# TP3 : Progressons vers le réseau d'infrastructure

## I. (mini)Architecture réseau

Pour calculer les masques de nos différents réseaux et pour les attribuer correctement, il faut prendre en compte que nous avons besoin du nombre de clients maximum ET de rajouter deux IP pour l'adresse de broadcast et l'adresse réseau.

* `client1` 35 clients max + 2 = 37, 37 ne rentrent pas dans un `/27`, mais dans un `/26`

* `server1` 63 serveurs max + 2 = 65, 65 ne rentrent pas dans un `/26`, mais dans un `/25`

* `server2` 10 serveurs max + 2 = 12, 12 ne rentrent pas dans un `/29` mais dans un `/28`

On met donc `server1` qui doit contenir 128 adresses (avec broadcast et adresse réseau) donc le réseau contiendra des adresses de 10.3.0.0 à 10.3.0.127

Ensuite on place le réseau `client1` à la suite de `server1` qui doit contenir 64 adresses donc le réseau contiendra des adresses de 10.3.0.128 à 10.3.0.191

Et enfin `server2` apres `client1`, qui dois contenir 16 adresses donc le réseau contiendra des adresses de 10.3.0.192 à 10.3.0.207

### 2. Routeur

### 🌞 Création du `router.tp3`


```bash
# Les différentes IPs 
[mattox@router network-scripts]$ ip a
(...)
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:2f:05:22 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.126/25 brd 10.3.0.127 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe2f:522/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:3f:de:e3 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.190/26 brd 10.3.0.191 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe3f:dee3/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:50:ee:b0 brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.206/28 brd 10.3.0.207 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe50:eeb0/64 scope link
       valid_lft forever preferred_lft forever

# Un acces à internet 
[mattox@router network-scripts]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=115 time=635 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=115 time=34.8 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=115 time=35.7 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 3 received, 25% packet loss, time 3040ms
rtt min/avg/max/mdev = 34.758/235.318/635.461/282.944 ms
-------------------------------------------------------
[mattox@router network-scripts]$ ping google.com
PING google.com (172.217.171.238) 56(84) bytes of data.
64 bytes from mrs09s07-in-f14.1e100.net (172.217.171.238): icmp_seq=1 ttl=111 time=166 ms
64 bytes from mrs09s07-in-f14.1e100.net (172.217.171.238): icmp_seq=2 ttl=111 time=733 ms
64 bytes from mrs09s07-in-f14.1e100.net (172.217.171.238): icmp_seq=3 ttl=111 time=632 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2001ms
rtt min/avg/max/mdev = 165.528/510.251/732.952/247.197 ms


# La résolution de noms
[mattox@router ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8
(...)
DNS1=1.1.1.1

# Le nom
[mattox@router ~]$ hostname
router.tp3

# Le routage
[mattox@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

# II. Services d'infra

## 1. Serveur DHCP

#### 🌞 Mettre en place une machine qui fera office de serveur DHCP dans le réseau `client1`

On lui donne le nom de `dhcp.client1.tp3`

```
[mattox@dhcp ~]$ hostname
dhcp.client1.tp3
```
Et on donne une IP, l'adresse d'un DNS et l'adresse de la passerelle
On commence à donner des IP à partir de `10.3.0.130` car l'adresse IP `10.3.0.129` est déjà prise par `dhcp.client1.tp3`
```
[mattox@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#

subnet 10.3.0.128 netmask 255.255.255.192 {
  range 10.3.0.130 10.3.0.189;
  option domain-name-servers 1.1.1.1;
  option routers 10.3.0.190;
}
```

Evidemment, il ne faut pas oublier de demarrer le dhcp 
```
[mattox@dhcp ~]$ sudo systemctl status dhcpd.service
● dhcpd.service - DHCPv4 Server Daemon
   Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-09-28 21:56:28 CEST; 5s ago
   (...)
```

On récupere le fichier de configuration `dhcpd.conf` 
=> [Fichier de configuration](./dhcpd.conf)

#### 🌞 Mettre en place un client dans le réseau client1

On nomme le client `marcel.client1.tp3`

```
[mattox@marcel ~]$ hostname
marcel.client1.tp3
```

`marcel` a récuperer une adresse grâce au DHCP :

```
[mattox@dhcp ~]$ sudo systemctl status dhcpd.service
[sudo] password for mattox:
● dhcpd.service - DHCPv4 Server Daemon
   Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-09-28 21:56:28 CEST; 55min ago
(...)
Sep 28 22:03:45 dhcp.client1.tp3 dhcpd[24297]: DHCPREQUEST for 10.3.0.130 from 08:00:27:8b:c0:b5 via enp0s8
Sep 28 22:03:45 dhcp.client1.tp3 dhcpd[24297]: DHCPACK on 10.3.0.130 to 08:00:27:8b:c0:b5 (marcel) via enp0s8
```
`marcel` a donc récuperer l'adresse de la passerelle

```
[mattox@marcel ~]$ ip r s
default via 10.3.0.190 dev enp0s8 proto dhcp metric 100
10.3.0.128/26 dev enp0s8 proto kernel scope link src 10.3.0.130 metric 100
```

Et le DNS 
```
[mattox@marcel ~]$ cat /etc/resolv.conf
(...)
nameserver 1.1.1.1
```
#### 🌞 Depuis marcel.client1.tp3

`marcel` a un acces vers internet et une résolution de noms
```bash
[mattox@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=35.7 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=35.10 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=52.3 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 35.684/41.308/52.256/7.742 ms
# On a acces a internet

---------------------------
[mattox@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 31318
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             155     IN      A       216.58.198.206

;; Query time: 35 msec

# DNS = 1.1.1.1 Résolution de noms
;; SERVER: 1.1.1.1#53(1.1.1.1)

;; WHEN: Tue Sep 28 22:58:22 CEST 2021
;; MSG SIZE  rcvd: 55
```
J'ai montré plus haut que `marcel` avait récupérer les informations du DHCP

A l'aide de la commande `traceroute` on peut voir que `marcel` utilise la passerelle pour se connecter, et donc `router.tp3`

```
[mattox@marcel ~]$ traceroute google.com
traceroute to google.com (142.250.75.238), 30 hops max, 60 byte packets
 1  _gateway (10.3.0.190)  0.701 ms  0.668 ms  0.652 ms
 2  10.0.2.2 (10.0.2.2)  0.594 ms  0.571 ms  0.586 ms
 3  10.0.2.2 (10.0.2.2)  15.041 ms  14.942 ms  14.840 ms
```

## 2. Serveur DNS

### B. SETUP copain

#### 🌞 Mettre en place une machine qui fera office de serveur DNS

```bash
# Il porte bien le nom dns1.server1.tp3
[mattox@dns1 ~]$ hostname
dns1.server1.tp3

# Il est bien situé dans le server1
[mattox@dns1 ~]$ ip a | grep enp0s8 | grep inet
    inet 10.3.0.1/25 brd 10.3.0.127 scope global noprefixroute enp0s8

# /etc/resolv.conf contient un DNS public connu
[mattox@dns1 ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8 | grep DNS
DNS1=1.1.1.1
```
**Mise en place du DNS**

```bash
# On commence par installer le paquet "bind"
[mattox@dns1 ~]$ dnf install -y bind bind-utils
(...)
Installed:
  bind-32:9.11.26-4.el8_4.x86_64

Complete!

# On configure notre fichier "/etc/named.conf
[mattox@dns1 ~]$ sudo nano /etc/named.conf
[mattox@dns1 ~]$ sudo cat /etc/named.conf
(...)
options {
         # on autorise les écoutes
        listen-on port 53 { any; };
        listen-on-v6 port 53 { any; };
         (...)
        allow-query     { any; };
         (...)
         # on autorise la récursion
        recursion yes;
(...)
# On ajoute des fichiers de zones pour "server1" et "server2"
zone "server1.tp3" IN {
        type master;
        file "/var/named/server1.tp3.forward";
        allow-update { none; };
};

zone "server2.tp3" IN {
        type master;
        file "/var/named/server2.tp3.forward";
        allow-update { none; };
};
include "/etc/named.rfc1912.zones";
include "/etc/named.root.key";

# On crée ensuite les deux fichiers de zones 
[mattox@dns1 ~]$ sudo nano /var/named/server1.tp3.forward
[mattox@dns1 ~]$ sudo cat /var/named/server1.tp3.forward
$TTL 86400
@ IN  SOA     dns1.server1.tp3. root.server1.tp3. (
         2021080804          ;Serial
         3600        ;Refresh
         1800        ;Retry
         604800      ;Expire
         86400 )     ;Minimum TTL
        ; Set your Name Servers here
@         IN  NS      dns1.server1.tp3.
         ; define Name Servers IP address
@         IN  A       10.3.0.1
; Set each IP address of a hostname. Sample A records.
dns1 IN A 10.3.0.1
router IN A 10.3.0.126

# On cree le fichier de zone pour "server2" qui est similaire sauf pour l'ip du router
[mattox@dns1 ~]$ sudo cat /var/named/server2.tp3.forward | grep router
router IN A 10.3.0.206

# On verifie ensuite que nos fichiers de zone soit corrects
[mattox@dns1 ~]$ sudo named-checkzone dns1.server1.tp3 /var/named/server1.tp3.forward
zone dns1.server1.tp3/IN: loaded serial 2021080804
OK

# Etant donné qu'il n'y a pas d'erreurs, 
# on configure le module "named" pour qu'il demarre automatiquement
[mattox@dns1 ~]$ sudo systemctl enable named

# On autorise le dns par le pare-feu
[mattox@dns1 ~]$ sudo firewall-cmd --add-service=dns --permanent
[mattox@dns1 ~]$ sudo firewall-cmd --reload
```

#### 🌞 Tester le DNS depuis marcel.client1.tp3

```bash
# On ajoute manuellement notre DNS
[mattox@marcel ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
[mattox@marcel ~]$ sudo cat /etc/etc/sysconfig/network-scripts/ifcfg-enp0s8 | grep DNS
DNS1=10.3.0.1

# On teste notre zone forward sur notre "dns1.server1.tp3"
[mattox@marcel ~]$ dig dns1.server1.tp3
(...)
;; QUESTION SECTION:
;dns1.server1.tp3.              IN      A

;; ANSWER SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.0.1

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; SERVER: 10.3.0.1#53(10.3.0.1)
(...)

# On voit bien que c'est notre propre serveur DNS
[mattox@marcel ~]$ dig dns1.server1.tp3 | grep SERVER
;; SERVER: 10.3.0.1#53(10.3.0.1)

# On fais de meme avec notre router
[mattox@marcel ~]$ dig router.server1.tp3
(...)
;; QUESTION SECTION:
;router.server1.tp3.            IN      A

;; ANSWER SECTION:
router.server1.tp3.     86400   IN      A       10.3.0.126

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; ADDITIONAL SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.0.1
(...)

# Et c'est encore notre serveur DNS
[mattox@marcel ~]$ dig router.server1.tp3 | grep SERVER
;; SERVER: 10.3.0.1#53(10.3.0.1)
```

#### 🌞 Configurez l'utilisation du serveur DNS sur TOUS vos noeuds

```bash
# On configure le DNS sur "dhcp.client1.tp3"
[mattox@dhcp ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
[mattox@dhcp ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8 | grep DNS
DNS1=10.3.0.1

# On vérifie que le DNS marche
[mattox@dhcp ~]$ dig dns1.server1.tp3
(...)
;; ANSWER SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.0.1

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; SERVER: 10.3.0.1#53(10.3.0.1)

# On fait de même sur le router
[mattox@router ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
[mattox@router ~]$ cat /etc/sysconfig/network-scripts/ifcfg-enp0s8 | grep DNS
DNS1=10.3.0.1

# Vérification
[mattox@router ~]$ dig dns1.server1.tp3
(...)
;; ANSWER SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.0.1

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; SERVER: 10.3.0.1#53(10.3.0.1)
```

## 3. Get deeper

### A. DNS forwarder

#### 🌞 Affiner la configuration du DNS

```bash
# Pour que notre DNS soit un forwarder DNS,
# il faut activer le parametre "recursion"
[mattox@dns1 ~]$ sudo nano /etc/named.conf
[mattox@dns1 ~]$ sudo cat /etc/named.conf | grep recursion
(...)
        recursion yes;
```

#### 🌞 Test !

```bash
# On regarde que "dig google.com" fonctionne
[mattox@marcel ~]$ dig google.com
(...)
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             300     IN      A       216.58.204.110

;; AUTHORITY SECTION:
google.com.             162252  IN      NS      ns1.google.com.
google.com.             162252  IN      NS      ns4.google.com.
google.com.             162252  IN      NS      ns3.google.com.
google.com.             162252  IN      NS      ns2.google.com.

;; ADDITIONAL SECTION:
ns2.google.com.         162252  IN      A       216.239.34.10
ns1.google.com.         162252  IN      A       216.239.32.10
ns3.google.com.         162252  IN      A       216.239.36.10
ns4.google.com.         162252  IN      A       216.239.38.10
ns2.google.com.         162252  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         162252  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         162252  IN      AAAA    2001:4860:4802:36::a
ns4.google.com.         162252  IN      AAAA    2001:4860:4802:38::a

;; Query time: 204 msec
;; SERVER: 10.3.0.1#53(10.3.0.1)
;; WHEN: Sun Oct 03 18:11:06 CEST 2021
;; MSG SIZE  rcvd: 331

# On verifie que c'est notre propre DNS
[mattox@marcel ~]$ dig google.com | grep SERVER
;; SERVER: 10.3.0.1#53(10.3.0.1)

# Cela est possible car dns1.server1.tp3 connait un DNS connu
[mattox@dns1 ~]$ sudo cat /etc/resolv.conf
search server1.tp3
nameserver 1.1.1.1
```

### B. On revient sur la conf du DHCP

#### 🌞 Affiner la configuration du DHCP

```bash
# On modifie la configuration du DHCP pour donner notre propre DNS
[mattox@dhcp ~]$ sudo nano /etc/dhcp/dhcpd.conf
[mattox@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf

subnet 10.3.0.128 netmask 255.255.255.192 {
  range 10.3.0.130 10.3.0.189;
  option domain-name-servers 10.3.0.1;
  option routers 10.3.0.190;
}

# Cette ligne montre le DNS qui est donné
[mattox@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf | grep domain-name-servers
  option domain-name-servers 10.3.0.1;

# Le DNS a bien donné les informations à johnny.client1.tp3
[mattox@dhcp ~]$ sudo systemctl status dhcpd.service
(...)
Oct 03 19:29:40 dhcp.client1.tp3 dhcpd[1761]: DHCPREQUEST for 10.3.0.131 from 08:00:27:4c:5c:cf (johnny) vi>
Oct 03 19:29:40 dhcp.client1.tp3 dhcpd[1761]: DHCPACK on 10.3.0.131 to 08:00:27:4c:5c:cf (johnny) via enp0s8

# On regarde sur johnny.client1.tp3 si les changements ont bien été effectué
[mattox@johnny ~]$ ip a
(...)
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:4c:5c:cf brd ff:ff:ff:ff:ff:ff
    inet 10.3.0.131/26 brd 10.3.0.191 scope global dynamic noprefixroute enp0s3
       valid_lft 39299sec preferred_lft 39299sec
    inet6 fe80::a00:27ff:fe4c:5ccf/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
# Il a bien récupéré l'ip 10.3.0.131 par le DHCP

# Il a bien recupéré l'adresse de la passerelle en route par defaut
[mattox@johnny ~]$ ip r s
default via 10.3.0.190 dev enp0s3 proto dhcp metric 100
10.3.0.128/26 dev enp0s3 proto kernel scope link src 10.3.0.131 metric 100

# Et il a bien récupéré notre DNS
[mattox@johnny ~]$ sudo cat /etc/resolv.conf
# Generated by NetworkManager
search client1.tp3
nameserver 10.3.0.1
```

## III. Services métier

### 1. Serveur Web

#### 🌞 Setup d'une nouvelle machine, qui sera un serveur Web, une belle appli pour nos clients
```bash
# Notre machine s'appelle bien "web1.server2.tp3"
[mattox@web1 ~]$ hostname
web1.server2.tp3

# On crée un serveur web
[mattox@web1 ~]$ sudo vim /etc/systemd/system/web.service
[mattox@web1 ~]$ cat /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target

[mattox@web1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent

[mattox@web1 ~]$ sudo firewall-cmd --reload

[mattox@web1 ~]$ sudo systemctl daemon-reload

[mattox@web1 ~]$ sudo systemctl start web
[mattox@web1 ~]$ sudo systemctl enable web
[mattox@web1 ~]$ sudo systemctl status web
● web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-04 22:58:32 CEST; 3min 5s ago
 Main PID: 24269 (python3)
    Tasks: 1 (limit: 11397)
   Memory: 10.3M
   CGroup: /system.slice/web.service
           └─24269 /bin/python3 -m http.server 8888

[mattox@web1 ~]$ curl localhost:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
(...)
# On a maintenant un serveur web qui marche correctement
```

#### 🌞 Test test test et re-test

```bash
# On essaye de curl depuis marcel
[mattox@marcel ~]$ curl web1.server2.tp3:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
# et ca marche parfaitement
```

### 2. Partage de fichiers

```bash
# On installe les ressources pour nfs
[mattox@nfs1 ~]$ sudo dnf -y install nfs-utils
(...)
Installed:
  gssproxy-0.8.0-19.el8.x86_64                      keyutils-1.5.10-6.el8.x86_64
  libevent-2.1.8-5.el8.x86_64                       libverto-libevent-0.3.0-5.el8.x86_64
  nfs-utils-1:2.3.3-41.el8_4.2.x86_64               rpcbind-1.2.5-8.el8.x86_64

Complete!

# On cree le dossier /srv/nfs_share/
[mattox@nfs1 ~]$ sudo mkdir /srv/nfs_share/

# On insere notre nom de domaine
[mattox@nfs1 ~]$ sudo vi /etc/idmapd.conf
[mattox@nfs1 ~]$ sudo cat /etc/idmapd.conf | grep Domain
Domain = server2.tp3

# On cree le partage
[mattox@nfs1 ~]$ sudo vi /etc/exports
[mattox@nfs1 ~]$ sudo cat /etc/exports
/srv/nfs_share 10.3.0.192/28(rw)

# Pour que nfs soit démarré automatiquement
[mattox@nfs1 ~]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.

# On autorise nfs sur le firewall
[mattox@nfs1 ~]$ sudo firewall-cmd --add-service=nfs --permanent
success
[mattox@nfs1 ~]$ sudo firewall-cmd --reload
success
```

#### 🌞 Configuration du client NFS

```bash
# On installe les ressources sur web1
[mattox@web1 ~]$ sudo dnf -y install nfs-utils
(...)
Complete!

# On insere notre nom de domaine
[mattox@web1 ~]$ sudo vi /etc/idmapd.conf
[mattox@web1 ~]$ sudo cat /etc/idmapd.conf | grep Domain
Domain = server2.tp3

# On monte sur /srv/nfs/
[mattox@web1 ~]$ sudo mount -t nfs nfs1.server2.tp3:/srv/nfs_share/ /srv/nfs/

# NFS est bien monté
[mattox@web1 ~]$ df -hT | grep nfs
nfs1.server2.tp3:/srv/nfs_share nfs4      6.2G  2.2G  4.1G  34% /srv/nfs

# On monte automatiquement au démarrage le partage NFS
[mattox@web1 ~]$ sudo vi /etc/fstab
[mattox@web1 ~]$ sudo cat /etc/fstab | grep nfs
nfs1.server2.tp3:/srv/nfs_share/ /srv/nfs/      nfs     defaults        0 0
```

#### 🌞 TEEEEST

```bash
# On crée un fichier depuis web1 dans le partage NFS
[mattox@web1 ~]$ sudo touch /srv/nfs/Fichiertest

# On écrit dedans pour voir si nfs1 pourra le lire
[mattox@web1 ~]$ sudo vi /srv/nfs/Fichiertest
[mattox@web1 ~]$ sudo cat /srv/nfs/Fichiertest
Ceci est un test
Il devra être lu par nfs1.server2.tp3

# On regarde du coté de nfs1, et le fichier est là
[mattox@nfs1 ~]$ ls -al /srv/nfs_share/
total 4
drwxr-xr-x. 2 root root 25 Oct  5 00:18 .
drwxr-xr-x. 3 root root 23 Oct  4 23:43 ..
-rw-r--r--. 1 root root 56 Oct  5 00:18 Fichiertest

# On essaye de le lire
[mattox@nfs1 ~]$ cat /srv/nfs_share/Fichiertest
Ceci est un test
Il devra être lu par nfs1.server2.tp3
# Et cela marche parfaitement
```

## IV. Un peu de théorie : TCP et UDP

#### 🌞 Déterminer, pour chacun de ces protocoles, s'ils sont encapsulés dans du TCP ou de l'UDP :

* SSH

```bash
# Pour capture une connexion SSH il suffit juste de se connecter en SSH
PS C:\Users\matte> ssh mattox@10.3.0.130
mattox@10.3.0.130s password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Tue Oct  5 16:32:36 2021 from 10.3.0.189
[mattox@marcel ~]$
```

D'apres la capture de trame `tp3_ssh.pcap`, on peut voir que la connexion SSH utilise du **TCP**. La connexion SSH assure un acces permanent, donc elle a besoin de s'assurer de la bonne réception des données.

-> [Le fichier SSH](./tp3_ssh.pcap)

* HTTP

```bash
# Pour capture un protocole HTTP il suffit juste de curl notre serveur web
[mattox@marcel ~]$ curl web1.server2.tp3:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
(...)
```

D'apres la capture de trame `tp3_http.pcap`, on peut voir que le protocole HTTP utilise du **TCP**. Il serait difficile de l'exécuter sur UDP sans la garantie de l'arrivée des paquets. Si les paquets n'arrivent pas, les données cryptées ne seront pas déchiffrables.

-> [Le fichier HTTP](./tp3_http.pcap)

* DNS
  
```bash
# Pour capture un protocole DNS il suffit juste de dig des noms de domaines
[mattox@marcel ~]$ dig google.com
[mattox@marcel ~]$ dig dns1.server1.tp3
```

D'apres la capture de trame `tp3_dns.pcap`, on peut voir que le protocole DNS utilise du **UDP**. En effet, UDP est un protocole simple qui permet d'avoir une réponse rapide. Pour le cas du DNS, on cherche juste à savoir une correspondance entre une IP et un nom de domaine, cela doit donc se faire rapidement.

-> [Le fichier DNS](./tp3_dns.pcap)

* NFS

```bash
# Pour capture un protocole NFS il suffit juste de creer un fichier sur le partage NFS
[mattox@nfs1 ~]$ sudo touch /srv/nfs_share/fichierdetest
```

D'apres la capture de trame `tp3_nfs.pcap`, on peut voir que le protocole NFS utilise du **TCP**. En effet le protocole NFS est un partage de fichiers, nous avons besoin d'être sûre que le fichier est bien arrivé.

-> [Le fichier NFS](./tp3_nfs.pcap)

#### 🌞 Capturez et mettez en évidence un 3-way handshake

Dans notre capture HTTP, on peut retrouver notre 3-way handshake.

-> [Le fichier 3 way handhsake](./tp3_3way.pcap)

## V. El final

#### 🌞 Bah j'veux un schéma.


![](./schema.png)


=> [Lien du schema](./schema.png)

#### 🌞 Et j'veux des fichiers aussi, tous les fichiers de conf du DNS

=> [Fichier de configuation](./named.conf)

=> [Fichier de zone server1](./server1.tp3.forward)

=> [Fichier de zone server2](./server2.tp3.forward)

#### 🌞 🗃️ Tableau des réseaux 🗃️

| Nom du réseau | Adresse du réseau | Masque            | Nombre de clients possibles | Adresse passerelle | Adresse broadcast |
| ------------- | ----------------- | ----------------- | --------------------------- | ------------------ | ----------------- |
| `client1`     | `10.3.0.128`      | `255.255.255.192` | 62                          | `10.3.0.190`       | `10.3.0.191`      |
| `server1`     | `10.3.0.0`        | `255.255.255.128` | 126                         | `10.3.0.126`       | `10.3.0.127`      |
| `server2`     | `10.3.0.192`      | `255.255.255.240` | 14                          | `10.3.0.206`       | `10.3.0.207`      |

#### 🌞 🗃️ Tableau d'adressage 🗃️

| Nom machine  | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
| ------------ | -------------------- | -------------------- | -------------------- | --------------------- |
| `router.tp3` | `10.3.0.190/26`         | `10.3.0.126/25`         | `10.3.0.206/28`         | Carte NAT             |
| `dhcp.client1.tp3`          | `10.3.0.129/26`                  | X                  | X                  | `10.3.0.190/26`          |
| `marcel.client1.tp3`          | `10.3.0.130/26`                  | X                  | X                  | `10.3.0.190/26`          |
| `dns1.server1.tp3`          | X                  | `10.3.0.1/25`                  | X                  | `10.3.0.126/25`          |
| `johnny.client1.tp3`          | `10.3.0.131/26`                  | X                  | X                  | `10.3.0.190/26`          |
| `web1.server2.tp3`          | X                  | X                  | `10.3.0.195/28`                  | `10.3.0.206/28`          |
| `nfs1.server2.tp3`          | X                  | X                  | `10.3.0.194/28`                  | `10.3.0.206/28`          |


