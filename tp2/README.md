# TP2 : On va router des trucs

- [TP2 : On va router des trucs](#tp2--on-va-router-des-trucs)
  - [I. ARP](#i-arp)
    - [1. Echange ARP](#1-echange-arp)
      - [🌞 Générer des requêtes ARP](#-générer-des-requêtes-arp)
    - [2. Analyse de trames](#2-analyse-de-trames)
      - [🌞 Analyse de trames](#-analyse-de-trames)
  - [II. Routage](#ii-routage)
    - [1. Mise en place du routage](#1-mise-en-place-du-routage)
    - [2. Analyse de trames](#2-analyse-de-trames-1)
    - [3. Accès internet](#3-accès-internet)
      - [Donnez un accès internet à vos machines](#donnez-un-accès-internet-à-vos-machines)
  - [III. DHCP](#iii-dhcp)
    - [Améliorer la configuration du DHCP](#améliorer-la-configuration-du-dhcp)
    - [2. Analyse de trame](#2-analyse-de-trame)

## I. ARP

### 1. Echange ARP

#### 🌞 Générer des requêtes ARP

Si l'on effectue des pings d'une machine à une autre : 

```
[mattox@node2 ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=64 time=1.51 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=64 time=0.371 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=64 time=0.888 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=64 time=0.956 ms
^C
--- 10.2.1.11 ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5060ms
rtt min/avg/max/mdev = 0.371/0.924/1.507/0.328 ms

----------------------------------------------------------------

[mattox@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.958 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.915 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.865 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=0.954 ms
^C
--- 10.2.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3004ms
rtt min/avg/max/mdev = 0.865/0.923/0.958/0.037 ms
```

On affiche donc la table ARP avec la commande `arp -a`

```
[mattox@node1 ~]$ ip n s
10.2.1.12 dev enp0s8 lladdr 08:00:27:f5:45:a5 REACHABLE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:46 REACHABLE

----------------------------------------------------------------

[mattox@node2 ~]$ ip n s
10.2.1.11 dev enp0s8 lladdr 08:00:27:b4:97:e4 REACHABLE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:46 REACHABLE
```
On peux voir que sur la table ARP de `node2`, on trouve l'adresse MAC de `node1` (`08:00:27:b4:97:e4`) grâce à son adresse IP.
De même, l'adresse MAC de `node2` (`08:00:27:f5:45:a5`) se trouve sur la table ARP de `node1`

En effet on peux vérifier ça en faisant un `ip a` sur `node1` et en affichant la table ARP sur `node2`

```
[mattox@node1 ~]$ ip a
(...)
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b4:97:e4 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.11/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feb4:97e4/64 scope link noprefixroute
       valid_lft forever preferred_lft forever

----------------------------------------------------------------

[mattox@node2 ~]$ ip n s
10.2.1.11 dev enp0s8 lladdr 08:00:27:b4:97:e4 REACHABLE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:46 REACHABLE
```

On peux voir qu'ils ont tous les deux la même adresse MAC `08:00:27:b4:97:e4`

### 2. Analyse de trames

#### 🌞 Analyse de trames

On vide d'abord la table ARP des deux machines avec `sudo ip neigh flush all`

Pour réaliser la capture de trame et la sauvegarder dans un fichier `tp2_arp.pcap` on utilise la commmande `sudo tcpdump -i enp0s8 -w tp2_arp.pcap not port 22`

En effet `-w tp2_arp.pcap` pour enregistrer la trame dans le fichier `tp2_arp.pcap`
et `not port 22` pour ne pas ecouter sur le port où se trouve la connexion `SSH`

```
[mattox@node2 ~]$ sudo tcpdump -i enp0s8 -w tcpdump.pcap not port 22
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C24 packets captured
24 packets received by filter
0 packets dropped by kernel
```
La commande sauvegarde automtiquement les données dans `tp2_arp.pcap`

Pour recuperer notre fichier, nous devons ouvrir un serveur web, par exemple en ouvrant le port 1234 avec la commande `sudo firewall-cmd --add-port=1234/tcp`.

Et on lance le serveur grâce au module python `python3 -m http.server 1234`

Et on recupere le fichier sur notre Hôte (Windows pour moi)
 
* On met en évidence les trames ARP 

| ordre | type trame  | source                      | destination                 |
| ----- | ----------- | --------------------------- | --------------------------- |
| 1     | Requête ARP | `node2` `08:00:27:f5:45:a5` | Broadcast `FF:FF:FF:FF:FF`  |
| 2     | Réponse ARP | `node1` `08:00:27:b4:97:e4` | `node2` `08:00:27:f5:45:a5` |
| 3     | Requête ARP | `node1` `08:00:27:b4:97:e4` | `node2` `08:00:27:f5:45:a5` |
| 4     | Réponse ARP | `node2` `08:00:27:f5:45:a5` | `node1` `08:00:27:b4:97:e4` |

## II. Routage

### 1. Mise en place du routage

* Tout d'abord il faut activer le routage sur la VM `router`

On repere la zone utilisée par le `firewalld`, ici `public`
```
[mattox@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8 enp0s9
```

Ensuite, on active le masquerading
```
[mattox@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

* Ajoutons les routes statiques nécessaires pour que `node1.net1.tp2` et `marcel.net2.tp2` puissent se ping

On ajoute une route à `node1` :

```
[mattox@node1 ~]$ sudo ip route add 10.2.2.0/24 via 10.2.1.254 dev enp0s8
[mattox@node1 ~]$ ip r s
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.11 metric 101
10.2.2.0/24 via 10.2.1.254 dev enp0s8
```

Et aussi à `marcel` : 
```
[mattox@marcel ~]$ sudo ip route add 10.2.1.0/24 via 10.2.2.254 dev enp0s8
[mattox@marcel ~]$ ip r s
10.2.1.0/24 via 10.2.2.254 dev enp0s8
10.2.2.0/24 dev enp0s8 proto kernel scope link src 10.2.2.12 metric 101
```
Nous avons effectué ces modifications **temporairement**

Nous allons maintenant les changer définitivement

On se place dans le répertoire `/etc/sysconfig/network-scripts` et on crée un fichier `route-enp0s8`

Pour `node1`
```
[mattox@node1 network-scripts]$ sudo touch route-enp0s8
[mattox@node1 network-scripts]$ sudo nano route-enp0s8

10.2.2.0/24 via 10.2.1.254 dev enp0s
```
Pour `marcel`
```
[mattox@marcel network-scripts]$ sudo touch route-enp0s8
[mattox@marcel network-scripts]$ sudo nano route-enp0s8

10.2.1.0/24 via 10.2.2.254 dev enp0s8
```

On peut vérifier que ca marche en faisant des ping :

de `node1` vers `marcel`
```
[mattox@node1 network-scripts]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.67 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.87 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.81 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=1.75 ms
^C
--- 10.2.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 1.668/1.773/1.874/0.091 ms
```
de `marcel` vers `node1`
```
[mattox@marcel network-scripts]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=2.09 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=1.15 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=63 time=1.67 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=63 time=1.86 ms
^C
--- 10.2.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 1.150/1.693/2.093/0.348 ms
```
 
### 2. Analyse de trames

* On vide les tables ARP des trois noeuds avec la commande `sudo ip neigh flush all`

```
[mattox@node1 network-scripts]$ sudo ip neigh flush all
```

* On fais un ping de `node1` vers `marcel` 

```
[mattox@node1 /]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=9.75 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.31 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.87 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=1.78 ms
64 bytes from 10.2.2.12: icmp_seq=5 ttl=63 time=1.86 ms
64 bytes from 10.2.2.12: icmp_seq=6 ttl=63 time=1.88 ms
^C
--- 10.2.2.12 ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5006ms
rtt min/avg/max/mdev = 1.306/3.072/9.749/2.993 ms
```

* On regarde les tables ARP des trois noeuds

`node1`:
```
[mattox@node1 /]$ ip n s
10.2.1.254 dev enp0s8 lladdr 08:00:27:df:01:3f STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:16 DELAY
```
`marcel`:
```
[mattox@marcel network-scripts]$ ip n s
10.2.2.254 dev enp0s8 lladdr 08:00:27:b8:85:de STALE
10.2.2.1 dev enp0s8 lladdr 0a:00:27:00:00:1a DELAY
```
`router`:
```
[mattox@router ~]$ ip neigh show
10.2.1.11 dev enp0s8 lladdr 08:00:27:b4:97:e4 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:16 REACHABLE
10.2.2.12 dev enp0s9 lladdr 08:00:27:f5:45:a5 STALE
```
Nous avons lancé un ping de `node1` (`10.2.1.11`), qui a donc passé par le `router` (`10.2.1.254`) comme on peux le voir sur la table ARP de `node1`, car `marcel` n'est pas dans le même réseau que `node1`. 

Ce ping a donc traversé le `router` de `10.2.1.254` à `10.2.2.254` (table ARP `router`) pour changer de réseau.
qui lui as donc à son tour transmis à `marcel` (`10.2.2.12`)

On capture les 3 capture grace à la commande `sudo tcpdump -i enp0s8 -w <fichier>.pcap not port 22`

* **Table ARP**

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | `node1` `08:00:27:b4:97:e4`  | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | `router` `08:00:27:df:01:3f` | x              | `node1` `08:00:27:b4:97:e4`   |
| 3     | Requête ARP | x         | `router` `08:00:27:b8:85:de` | x              | Broadcast `FF:FF:FF:FF:FF`   |
| 4     | Réponse ARP | x         | `marcel` `08:00:27:f5:45:a5` | x              | `router` `08:00:27:b8:85:de`   |
| 5     | Requête ARP | x         | `marcel` `08:00:27:f5:45:a5` | x              | `router` `08:00:27:b8:85:de`   |
| 6     | Réponse ARP | x         | `router` `08:00:27:b8:85:de` | x              | `marcel` `08:00:27:f5:45:a5`   |
| 7     | Requête ARP | x         | `router` `08:00:27:df:01:3f` | x              | `node1` `08:00:27:b4:97:e4`   |
| 8     | Réponse ARP | x         | `node1` `08:00:27:b4:97:e4` | x              | `router` `08:00:27:df:01:3f`   |

* **Pings**

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Ping | 10.2.1.11        |`node1` `08:00:27:b4:97:e4` | 10.2.2.12              | `router` `08:00:27:df:01:3f` |
| 2     | Ping | 10.2.1.11        | `router` `08:00:27:b8:85:de`  | 10.2.2.12              | `marcel` `08:00:27:f5:45:a5` |
| 3     | Pong | 10.2.2.12         | `marcel` `08:00:27:f5:45:a5`  | 10.2.1.11              | `router` `08:00:27:b8:85:de` |
| 4     | Pong | 10.2.2.12         | `router` `08:00:27:df:01:3f`  | 10.2.1.11              | `node1` `08:00:27:b4:97:e4` |

### 3. Accès internet

#### Donnez un accès internet à vos machines

* le routeur a déjà acces internet :

```
[mattox@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=115 time=23.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=115 time=24.5 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=115 time=22.5 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 22.539/23.444/24.463/0.799 ms
```

* On rajoute temporairement une route par défaut à `node1` et `marcel`

```
[mattox@node1 ~]$ sudo ip route add default via 10.2.1.254 dev enp0s8
---------------------
[mattox@marcel ~]$ sudo ip route add default via 10.2.2.254 dev enp0s8
```

Et on leur ajoute cette route définitivement

```
[mattox@node1 ~]$ sudo nano /etc/sysconfig/network

# Created by anaconda
GATEWAYS=10.2.1.254
------------------------------
[mattox@marcel ~]$ sudo nano /etc/sysconfig/network

# Created by anaconda
GATEWAYS=10.2.2.254
```

Et on vérifie si les machines ont accès à internet

```
[mattox@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=65.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=67.9 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=61.8 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=114 time=77.3 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 61.794/68.089/77.290/5.752 ms
```

* Mettons un DNS aux deux machines comme par exemple `1.1.1.1`

```
[mattox@node1 ~]$ sudo nano /etc/resolv.conf

# Generated by NetworkManager
search net1.tp2
nameserver 1.1.1.1
-------------------------------
[mattox@marcel ~]$ sudo nano /etc/resolv.conf

# Generated by NetworkManager
search net2.tp2
nameserver 1.1.1.1
```

On vérifie que la résolution de noms fonctionne avec `dig`

```
[mattox@node1 ~]$ dig 1.1.1.1

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> 1.1.1.1
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 43081
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;1.1.1.1.                       IN      A

;; AUTHORITY SECTION:
.                       86400   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2021092301 1800 900 604800 86400

;; Query time: 95 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Thu Sep 23 23:00:14 CEST 2021
;; MSG SIZE  rcvd: 111
```

On peut voir avec la commande dig que le DNS 1.1.1.1 fonctionne parfaitement

Et le ping aussi

```
[mattox@node1 ~]$ ping ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=51 time=64.9 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=51 time=59.8 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=3 ttl=51 time=67.3 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=4 ttl=51 time=53.8 ms
^C
--- ynov.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 53.800/61.421/67.255/5.167 ms
```

* Analyse de trames

On réalise un ping et on écoute sur un autre terminal :

```
[mattox@node1 ~]$ sudo tcpdump -i enp0s8 -w tp2_routage_internet.pcap not port 22
[sudo] password for mattox:
dropped privs to tcpdump
tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
^C29 packets captured
29 packets received by filter
0 packets dropped by kernel
----------------------------------
[mattox@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=28.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=32.5 ms
(...)
64 bytes from 8.8.8.8: icmp_seq=4 ttl=114 time=208 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3007ms
rtt min/avg/max/mdev = 28.578/90.606/207.777/72.392 ms
```



| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Ping | `node1` `10.2.1.11`       |`node1` `08:00:27:b4:97:e4` | `8.8.8.8`              | `router` `08:00:27:df:01:3f` |
| 2     | Pong | `8.8.8.8`        |`router` `08:00:27:df:01:3f` | `node1` `10.2.1.11`              | `node1` `08:00:27:b4:97:e4` |

## III. DHCP

Tout d'abord nous devons installe `dhcp-server`

```
[mattox@node1 ~]$ sudo dnf -y install dhcp-server
```

Nous devons modifier le fichier `/etc/dhcp/dhcpd.conf` pour configurer notre serveur DHCP

```
[mattox@node1 ~]$ sudo nano /etc/dhcp/dhcpd.conf


#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#

subnet 10.2.1.0 netmask 255.255.255.0 {
  # IP disponibles
  range 10.2.1.12 10.2.1.200;
}
```

Ensuite nous devons démarrer le DHCP 

```
[mattox@node1 ~]$ sudo systemctl start dhcpd.service
```

* On se place sur `node2.net1.tp2` et on lui attribue une IP avec le DHCP

```
[mattox@node2 ~]$ cd /etc/sysconfig/network-scripts
[mattox@node2 ~]$ sudo nano ifcfg-enp0s8

TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=dhcp
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
NAME=enp0s8
UUID=f6016371-9ca6-42ad-b5ad-d106353a872a
DEVICE=enp0s8
ONBOOT=yes

[mattox@node2 ~]$ sudo nmcli con reload
[mattox@node2 ~]$ sudo nmcli con up enp0s8
Connection successfully activated (D-Bus active path : /org/freedesktop/NetworkManager/ActiveConnection/3)
```

On peux voir que `node2` a récuperer l'adresse `10.2.1.12`

```
[mattox@node2 network-scripts]$ ip a
(...)
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f9:31:0b brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 42970sec preferred_lft 42970sec
    inet6 fe80::a00:27ff:fef9:310b/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

### Améliorer la configuration du DHCP

* Mettons une route par défault et un DNS (1.1.1.1) à utiliser

```
[mattox@node1 ~]$ sudo nano /etc/dhcp/dhcpd.conf

subnet 10.2.1.0 netmask 255.255.255.0 {
  # IP disponibles
  range 10.2.1.12 10.2.1.200;
  # DNS
  option domain-name-servers 1.1.1.1;
  # Route par défault
  option routers 10.2.1.254;
}
```

On rafraichit le DHCP, et on regarde si node2 peut ping sa passerelle

```
[mattox@node2 network-scripts]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=1.67 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=1.05 ms
64 bytes from 10.2.1.254: icmp_seq=3 ttl=64 time=0.470 ms
^C
--- 10.2.1.254 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2005ms
rtt min/avg/max/mdev = 0.470/1.062/1.668/0.489 ms
```

On peut voir que le DHCP lui as à nouveau donner l'adresse 10.2.1.12

```
[mattox@node2 network-scripts]$ ip a
(...)
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f9:31:0b brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 42310sec preferred_lft 42310sec
    inet6 fe80::a00:27ff:fef9:310b/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

On peut voir si `node2` a une route par défault avec la commande `ip r s`

```
[mattox@node2 network-scripts]$ ip r s
default via 10.2.1.254 dev enp0s8 proto dhcp metric 101
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.12 metric 101
```

On peut voir que le DNS marche parfaitement car `node2` peut ping `marcel` qui ne se trouve pas dans le même réseau que lui

```
[mattox@node2 network-scripts]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=2.62 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.81 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.85 ms
^C
--- 10.2.2.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 1.805/2.091/2.621/0.378 ms
```

On vérifie que `node2` connait la résolution de noms avec la commande `dig`

```
[mattox@node2 network-scripts]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 38036
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;google.com.                    IN      A

;; ANSWER SECTION:
google.com.             206     IN      A       216.58.198.206

;; Query time: 57 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Sun Sep 26 23:38:32 CEST 2021
;; MSG SIZE  rcvd: 55
```

Puis on ping un nom de domaine (`google.com`) pour montrer que tout marche correctement

```
[mattox@node2 network-scripts]$ ping google.com
PING google.com (216.58.213.142) 56(84) bytes of data.
64 bytes from par21s03-in-f14.1e100.net (216.58.213.142): icmp_seq=1 ttl=112 time=57.3 ms
64 bytes from par21s03-in-f14.1e100.net (216.58.213.142): icmp_seq=2 ttl=112 time=104 ms
64 bytes from par21s03-in-f14.1e100.net (216.58.213.142): icmp_seq=3 ttl=112 time=132 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 57.297/97.622/131.716/30.700 ms
```

### 2. Analyse de trame

Pour analyser les échanges on écoute les transactions sur `node1` et on deconnecte puis reconnecte la carte `enp0s8` sur `node2` pour demander au DHCP une nouvelle adresse IP

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requete DHCP | x |`node2` `08:00:27:f9:31:0b` | `255.255.255.255`              | Broadcast `ff:ff:ff:ff:ff:ff` |
| 2     | Reponse DHCP | `node1` `10.2.1.11`        |`node1` `08:00:27:b4:97:e4` | `node2` `10.2.1.12`              | `node2` `08:00:27:f9:31:0b` |